# OS OpenMap Mapbox Studio Style #
![screenshot](https://bytebucket.org/earthware/osopenmap-mapbox-studio/raw/8513c3277c2cb6f8b892bbfba8fd0715bded343a/preview.PNG)

This repository contains a [Mapbox Studio](https://www.mapbox.com/mapbox-studio) datasource and style for the [Ordnance Survey OS OpenMap](http://www.ordnancesurvey.co.uk/business-and-government/products/os-open-map-local.html) dataset. The style was created to mimic similar styles provided by Ordnance Survey for other mapping tools.

### Demo ###

The style can be seen in this [online demo](https://a.tiles.mapbox.com/v4/earthware.lje0ggl3/page.html?access_token=pk.eyJ1IjoiZWFydGh3YXJlIiwiYSI6InVoNUQzQXcifQ.QyYkOq_qGtg4sTSsSFBb2Q#16/50.9076/-1.367)

### Current Status ###

The style is complete but there are a number small issues and the potential for a lot of improvements especially around labeling and zoom levels, feel free to contribute.

### Directory Structure ###

* "OS OpenMap PostGis.tm2source" - Mapbox Studio tms2 datasource project loading data from local PostGIS database (not included)
* "PostgreSQL Queries" - PostgreSQL queries used on the raw data tables to create extra data required for styling
* "Raw Map Data" - example raw shp files for a small grid area in the UK 
* "style files" - the Mapbox Studio Style project containing CartoCSS files to style the datasource above. 

### How do I get set up? ###
1. Get your own up to date [Ordnance Survey OS OpenMap](http://www.ordnancesurvey.co.uk/business-and-government/products/os-open-map-local.html) data
2. Load your own data into a local PostgreSQL database see [these tutorials](https://www.youtube.com/playlist?list=PLBg6FOStFauXtuacucEqbkL3YseUAQKTT) if you do not know how to do that
3. Update the included "OS OpenMap PostGis.tm2source" to point to your local PostgreSQL database by changing each layers PostGIS settings (either using the user interface or using a text editor)
4. Change the datasource of the include style to be either pointing to your local data source project from 3 or your own uploaded Mapbox Dataset

### Who do I talk to? ###
Need more help or want to contribute [info@earthware.co.uk](mailto:info@earthware.co.uk)