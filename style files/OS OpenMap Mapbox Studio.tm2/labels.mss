@font_reg: "Arial Unicode MS Regular";
@font_bold: "Arial Unicode MS Bold";
@text-grey: #737373;

#roadlabels[zoom>12]{

  text-face-name:@font_bold;
  text-halo-radius:1;
  text-placement:line;
  text-name:"''";
  text-size:12;
  text-transform:uppercase;
  text-min-distance:300;
  text-avoid-edges: true;
  
  [classifica='Local Street'], [classifica='Local Street, Collapsed Dual Carriageway']
  {
    [zoom>=16]{
      text-name:"[distname]";
      text-fill:@text-grey;
      text-halo-fill:#fff;
      text-halo-radius: 2;  
    }
  }
  
  [classifica='Primary Road'], [classifica='Primary Road, Collapsed Dual Carriageway']{
    shield-fill: @text-grey;
    shield-name: [roadnumber];
    shield-face-name:@font_bold;
    shield-size:10;
    shield-file:  url("images/shields/primary-road.svg");
    shield-transform:scale(0,0);
    shield-spacing: 400;
    shield-min-distance: 300;
    shield-avoid-edges: true;
    shield-allow-overlap:false;
    shield-placement:line;    
    shield-halo-fill:white;
    shield-halo-radius:1;
    
    [zoom>12]{
      shield-halo-radius:0;
      shield-size:12;
      shield-fill: white;
      shield-transform:scale([roadnumber_length]/2.2,1.6);   
    }
  }  
  
  [classifica='A Road'], [classifica='A Road, Collapsed Dual Carriageway']{
    shield-fill: @text-grey;
    shield-name: [roadnumber];
    shield-face-name:@font_bold;
    shield-size:10;
    shield-file:  url("images/shields/a-road.svg");
    shield-transform:scale(0,0);
    shield-spacing: 400;
    shield-min-distance: 300;
    shield-avoid-edges: true;
    shield-allow-overlap:false;
    shield-placement:line;    
    shield-halo-fill:white;
    shield-halo-radius:1;
    
    [zoom>12]{
      shield-halo-radius:0;
      shield-size:12;
      shield-fill: white;
      shield-transform:scale([roadnumber_length]/2.2,1.6);   
    } 
  }
  [classifica='Motorway'], [classifica='Motorway, Collapsed Dual Carriageway']{
    shield-fill: @text-grey;
    shield-name: [roadnumber];
    shield-face-name:@font_bold;
    shield-size:10;
    shield-file:  url("images/shields/motorway.svg");
    shield-transform:scale(0,0);
    shield-spacing: 400;
    shield-min-distance: 300;
    shield-avoid-edges: true;
    shield-allow-overlap:false;
    shield-placement:line;    
    shield-halo-fill:white;
    shield-halo-radius:1;
    
    [zoom>12]{
      shield-halo-radius:0;
      shield-size:12;
      shield-fill: white;
      shield-transform:scale([roadnumber_length]/2.2,1.6);   
    }
  }  
}

#functionalsitelabel[zoom>14]{
  text-face-name:@font_bold;
  text-halo-radius:1;
  text-name:"''";
  text-size:16;
  text-avoid-edges: true;
  text-wrap-width: 30;
  [distname=~'.*School.*']{
    text-name:"'School'";
  }
  [distname=~'.*College.*']{
    text-name:"'College'";
  }
  [distname=~'.*University.*']{
    text-name:"'University'";
  }
  [distname=~'.*Hospital.*']{
    text-name:"'Hospital'";
  }
  [classifica=~'.*Bus Station.*']{
    text-name:"'Bus Station'";
  }
  [classifica=~'.*Coach Station.*']{
    text-name:"'Coach Station'";
  }
  [classifica=~'.*Railway.*']{
    text-name:"'Railway'";
  }  
}

#namedplace {
  text-fill:@text-grey;
  text-face-name:@font_reg;
  text-halo-radius:1;
  text-size:12;  
  text-name:"['']";
  text-wrap-width: 50;
  text-avoid-edges: true;
  
  // default font sizes
  [fontheight='Small'][zoom>15]{
    text-name:"[distname]";
    text-size:14;  
  }
  [fontheight='Medium'][zoom>13]{
    text-name:"[distname]";
    text-size:15; 
  }
  [fontheight='Large'][zoom>11]{
    text-name:"[distname]";
    text-size:17; 
    text-allow-overlap:true;
  } 
  
  // specific features
  [featcode=15803]{ // woodland
    [zoom<15]{
      text-name:"['']";
    }    
    text-name:"[distname]";
    text-fill:darken(@woodland, 50%);
  }
  [featcode=15804]{ // water
    [zoom<14]{
      text-name:"['']";
    }
    text-name:"[distname]";
    text-fill:darken(@water, 50%);
  }
  [featcode=15801]{ // populated place
    [fontheight='Large'][zoom>6]{
      text-name:"[distname]";
      text-size:17; 
      text-allow-overlap:true; 
    }
  }  
}


