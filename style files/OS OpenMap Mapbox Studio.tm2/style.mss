// colours
@water: #e4f4f7;
@waterdark: #5dc6de;
@woodland: #e3f2e2;
@foreshore: #eeefea;
@building: #f2e6d4;
@building-outline: #8f887f;
@glasshouse-line: #969687;

Map { 
  background-color: #fff;
  font-directory: url('');
}

