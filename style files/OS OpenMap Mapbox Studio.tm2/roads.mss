@standard-casing: #969696;
@minorroadfill:#fff4e1;
@aroadfill:#ffbbd2;
@broadfill:#fdd3b1;
@primaryroadfill:#a3d2a9;
@motorwayfill:#63c8dd;
@railwaycolor:#c8c4c0;

#roadtunnel {
  line-width: 1.5;
  line-dasharray: 4,3;
  line-color: @standard-casing;
}

#road {
  [classifica='Motorway'], [classifica='Motorway, Collapsed Dual Carriageway']{
    ::case {
      [zoom<=18]{
        line-width: 16;
      }  
      [zoom=16]{
        line-width: 10;
      }      
      [zoom=15]{
        line-width: 8;
      }         
      [zoom<=14]{
        line-width: 4;
      }
      [zoom=13]{
        line-width: 3;
        line-opacity:0.8;
      }  
      [zoom=12]{
        line-width: 3;
        line-opacity:0.5;
      }  
      [zoom<=11]{
        line-opacity:0;
      }      
      line-width: 8;
      line-color:@standard-casing;
    }        
    ::fill {
      [zoom<=18]{
        line-width: 12;
      }          
      [zoom=16]{
        line-width: 8;
      }    
      [zoom=15]{
        line-width: 6;
      }         
      [zoom<=14]{
        line-width: 2;
      }      
      [zoom<=13]{
        line-width: 1;
      }        
      line-width: 6;
      line-color:@motorwayfill;
      line-cap:round; 
      line-join:round; 

    }
    [classifica='Motorway, Collapsed Dual Carriageway']{
      line-width: 1;
      line-color:#fff;
      line-opacity:0.5;
    }
  }
  [classifica='A Road'], [classifica='A Road, Collapsed Dual Carriageway']{
    ::case {
      [zoom<=18]{
        line-width: 16;
      }   
      [zoom=16]{
        line-width: 10;
      }      
      [zoom=15]{
        line-width: 8;
      } 
      [zoom<=14]{
        line-width: 4;
      }
      [zoom=13]{
        line-width: 3;
        line-opacity:0.7;
      }  
      [zoom=12]{
        line-width: 3;
        line-opacity:0.5;
      }  
      [zoom<=11]{
        line-opacity:0;
      }      
      line-width: 8;
      line-color:@standard-casing;
      line-opacity:0.8;

    }        
    ::fill {
      [zoom<=18]{
        line-width: 12;
      }    
      [zoom=16]{
        line-width: 8;
      }    
      [zoom=15]{
        line-width: 6;
      }       
      [zoom<=14]{
        line-width: 2;
      }      
      [zoom<=13]{
        line-width: 1;
      }        
      line-width: 6;
      line-color:@aroadfill;
      line-cap:round; 
      line-join:round;      
    }
    [classifica='A Road, Collapsed Dual Carriageway']{
      line-width: 1;
      line-color:@standard-casing;
      line-opacity:0.5;
    }    
  }
  [classifica='Primary Road'], [classifica='Primary Road, Collapsed Dual Carriageway']{
    ::case {
      [zoom<=18]{
        line-width: 16;
      }   
      [zoom=16]{
        line-width: 10;
      }      
      [zoom=15]{
        line-width: 8;
      } 
      [zoom<=14]{
        line-width: 4;
      }
      [zoom=13]{
        line-width: 3;
        line-opacity:0.7;
      }  
      [zoom=12]{
        line-width: 3;
        line-opacity:0.5;
      }  
      [zoom<=11]{
        line-opacity:0;
      }      
      line-width: 8;
      line-color:@standard-casing;
      line-opacity:0.8;

    }        
    ::fill {
      [zoom<=18]{
        line-width: 12;
      }    
      [zoom=16]{
        line-width: 8;
      }    
      [zoom=15]{
        line-width: 6;
      }       
      [zoom<=14]{
        line-width: 2;
      }      
      [zoom<=13]{
        line-width: 1;
      }        
      line-width: 6;
      line-color:@primaryroadfill;
      line-cap:round; 
      line-join:round;      
    }
    [classifica='Primary Road, Collapsed Dual Carriageway']{
      line-width: 1;
      line-color:#ffffff;
      line-opacity:0.5;
    }    
  }
  [classifica='B Road'], [classifica='B Road, Collapsed Dual Carriageway']{
    ::case {
      [zoom<=18]{
        line-width: 16;
      }   
      [zoom=16]{
        line-width: 10;
      }      
      [zoom=15]{
        line-width: 8;
      } 
      [zoom<=14]{
        line-width: 4;
      }
      [zoom=13]{
        line-width: 3;
        line-opacity:0.7;
      }  
      [zoom=12]{
        line-width: 3;
        line-opacity:0.5;
      }  
      [zoom<=11]{
        line-opacity:0;
      }      
      line-width: 8;
      line-color:@standard-casing;
      line-opacity:0.8;

    }        
    ::fill {
      [zoom<=18]{
        line-width: 12;
      }    
      [zoom=16]{
        line-width: 8;
      }    
      [zoom=15]{
        line-width: 6;
      }       
      [zoom<=14]{
        line-width: 2;
      }      
      [zoom<=13]{
        line-width: 1;
      }        
      line-width: 6;
      line-color:@broadfill;
      line-cap:round; 
      line-join:round;      
    }
    [classifica='B Road, Collapsed Dual Carriageway']{
      line-width: 1;
      line-color:@standard-casing;
      line-opacity:0.5;
    }    
  } 
  
[classifica='Minor Road'], [classifica='Minor Road, Collapsed Dual Carriageway']{
    ::case {
      [zoom<=18]{
        line-width: 16;
      }   
      [zoom=16]{
        line-width: 10;
      }      
      [zoom=15]{
        line-width: 8;
      } 
      [zoom<=14]{
        line-width: 4;
      }
      [zoom=13]{
        line-width: 3;
        line-opacity:0.7;
      }  
      [zoom=12]{
        line-width: 3;
        line-opacity:0.5;
      }  
      [zoom<=11]{
        line-opacity:0;
      }      
      line-width: 8;
      line-color:@standard-casing;
      line-opacity:0.8;

    }        
    ::fill {
      [zoom<=18]{
        line-width: 12;
      }    
      [zoom=16]{
        line-width: 8;
      }    
      [zoom=15]{
        line-width: 6;
      }       
      [zoom<=14]{
        line-width: 2;
      }      
      [zoom<=13]{
        line-width: 1;
      }        
      line-width: 6;
      line-color:@minorroadfill;
      line-cap:butt; 
      line-join:round;      
    }
    // TODO minor road collapsed not working??
    [classifica='Minor Road, Collapsed Dual Carriageway']{
      line-width: 1;
      line-color:@standard-casing;
    }    
  } 
  
[classifica='Local Street'], [classifica='Local Street, Collapsed Dual Carriageway']{
    ::case {
      [zoom<=18]{
        line-width: 16;
      }   
      [zoom<=17]{
        line-width: 10;
      }      
      [zoom<=16]{
        line-width: 8;
        line-opacity:0.5;
      } 
      [zoom<=15]{
        line-width: 4;
        line-opacity:0.4;
      }
      [zoom=13]{
        line-width: 3;
        line-opacity:0.3;
      }  
      [zoom=12]{
        line-width: 3;
        line-opacity:0.2;
      }  
      [zoom<=11]{
        line-opacity:0;
      }      
      line-width: 8;
      line-color:@standard-casing;
      line-opacity:0.8;
      line-cap:round; 
    }        
    ::fill {
      [zoom<=18]{
        line-width: 12;
      }    
      [zoom<=17]{
        line-width: 8;
      }    
      [zoom<=16]{
        line-width: 6;
      }       
      [zoom<=15]{
        line-width: 2;
      }      
      [zoom<=13]{
        line-width: 1;
      }        
      line-width: 6;
      line-color:#fff;
      line-cap:round;      
    }
    [classifica='Local Street, Collapsed Dual Carriageway']{
      line-width: 1;
      line-color:@standard-casing;
    }    
  }     
}

#roundabout [zoom>14] {
  ::case {
      marker-fill:@aroadfill;
      marker-line-color:@standard-casing; 
      marker-allow-overlap:true;
      marker-width:50px;
      marker-line-width:0;
      marker-line-opacity:0.8;
      [zoom=17]{
        marker-width:40px;
      }
      [zoom=16]{
        marker-width:30px;
      }  
      [zoom=15]{
        marker-width:20px;
      }       
    }   
  ::fill {
      marker-line-color:@standard-casing;      
      marker-line-width:3;  
      marker-width:20px;
      marker-fill:white;
      marker-allow-overlap:true;
      marker-line-opacity:0.8;
      [zoom=17]{
        marker-line-width:2; 
        marker-width:15px;
      }  
      [zoom=16]{
        marker-line-width:1; 
        marker-width:10px;
      }    
      [zoom=15]{
        marker-line-width:1; 
        marker-width:5px;
      }        
  }   
  [classifica='Motorway'],[classifica='Motorway, Collapsed Dual Carriageway']{
    ::case{
      marker-fill:@motorwayfill; 
    }
  }
  [classifica='A Road'],[classifica='A Road, Collapsed Dual Carriageway']{  
      // uses the default
  }
  [classifica='Primary Road'],[classifica='Primary Road, Collapsed Dual Carriageway']{
    ::case{
      marker-fill:@primaryroadfill; 
    }
  } 
  [classifica='B Road'],[classifica='B Road, Collapsed Dual Carriageway']{
    ::case{
      marker-fill:@broadfill; 
      marker-transform:scale(0.8,0.8);
    }
    ::fill{   
      marker-transform:scale(0.8,0.8);
    }
  }
  [classifica='Minor Road'],[classifica='Minor Road, Collapsed Dual Carriageway']{
    ::case{
      marker-fill:@minorroadfill; 
      marker-transform:scale(0.8,0.8);
    }
    ::fill{   
      marker-transform:scale(0.8,0.8);
    }
  }
  [classifica='Local Street'],[classifica='Local Street, Collapsed Dual Carriageway']{
    // TODO improve styling
    ::case{
      marker-fill:#fff; 
      marker-line-width:1;
      marker-transform:scale(0.6,0.6);
    }
    ::fill{   
      marker-transform:scale(0.6,0.6);
    }
  }
}

#railwaytrack{
    line-width: 0.8;
    line-color: @railwaycolor;
    hatch/line-width: 3;
    hatch/line-color: @railwaycolor;
    hatch/line-dasharray: 1,15;  
}

#railwaytunnel {
  line-width: 1;
  line-color: #919191;
  line-dasharray: 5,5;   
  opacity:0.5;
}

#electricitytransmissionline {
    line-width: 0.8;
    line-color: @railwaycolor;
    line-dasharray: 25,5;  
}
