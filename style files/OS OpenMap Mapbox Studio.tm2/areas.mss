
#functionalsite {
  line-width: 2;
  line-opacity:0.25;
  polygon-opacity:0.25;  
  [sitetheme='Air Transport']{
    polygon-fill:#ffff7f;
    line-color: darken(#ffff7f,30%);
  }
  [sitetheme='Education']{
    polygon-fill:#db7fff;
    line-color: darken(#db7fff,30%);
  }
  
  [sitetheme='Medical Care']{
    polygon-fill:#7fff88;
    line-color: darken(#7fff88,30%);
  }
  [sitetheme='Rail Transport']{
    polygon-fill:#7fffee;
    line-color: darken(#7fffee,30%);
  }
  [sitetheme='Road Transport']{
    polygon-fill:#ff8a7f;
    line-color: darken(#ff8a7f,30%);
  }  
  [sitetheme='Water Transport']{
    polygon-fill:#7fa8ff;
    line-color: darken(#7fa8ff,30%);
  }   
}

#tidalboundary {
  line-width: 1;
  line-color: #5dc6de;
  text-face-name: 'Arial Unicode MS Regular';
  text-fill: #5dc6de;
  text-halo-fill: white;
  text-halo-radius: 1;
  text-size: 12;
  text-placement: line;  
  text-name: "MHW";
  text-min-distance: 130;
  text-dy: 5;
  text-avoid-edges: true;
  [classifica='Low Water Mark']{
    line-width: 0.5;
    text-name: "MLW";
  }
}

#surfacewater_line {
  line-width: 1;
  line-color: @waterdark;
}


#woodland {
  polygon-fill:@woodland;
}

#tidalwater {
  polygon-fill: @water;
}

#surfacewater_area {
  polygon-fill: @water;
}

#foreshore {
  polygon-fill:@foreshore;
}

#importantbuilding, #building {
  polygon-fill: @building;
  line-width: 1;
  line-color: @building-outline; 
  line-opacity:0.4;
  [zoom<15]{
    line-width: 0; 
  }
}

#glasshouse {
  polygon-fill: #fff;  
  line-width: 1;
  line-color: @glasshouse-line;
  // TODO cross hatach
}
