﻿-- Table: public.functionalsitecentroid

-- DROP TABLE public.functionalsitecentroid;

CREATE TABLE public.functionalsitecentroid
(
  ogc_fid integer NOT NULL DEFAULT nextval('functionalsite_ogc_fid_seq'::regclass),
  wkb_geometry geometry(Point,900914),
  id character varying(38),
  distname character varying(120),
  sitetheme character varying(21),
  classifica character varying(90),
  featcode double precision,
  CONSTRAINT functionalsitecentroid_pkey PRIMARY KEY (ogc_fid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.functionalsitecentroid
  OWNER TO postgres;

-- Index: public.functionalsitecentroid_wkb_geometry_geom_idx

-- DROP INDEX public.functionalsitecentroid_wkb_geometry_geom_idx;

CREATE INDEX functionalsitecentroid_wkb_geometry_geom_idx
  ON public.functionalsitecentroid
  USING gist
  (wkb_geometry);

